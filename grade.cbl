       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GRADE.
       AUTHOR.   WIMONSIRI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE  ASSIGN TO "mygrade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE  ASSIGN TO "AVGgrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL. 
             
       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DATAILS.
           88 END-OF-GRADE-FILE VALUE  HIGH-VALUE.
           05 SUB-ID     PIC X(6).
           05 SUB-NAME   PIC X(50).
           05 SUB-UNIT   PIC 9(1).
           05 GRADE      PIC X(2).

       FD  AVG-GRADE-FILE.    
       01  AVG-GRADE-DATAILS.
           05 SUBJECT-NAME   PIC X(20).
           05 AVG-GRADE PIC 9(1)V9(3) .

       WORKING-STORAGE SECTION.   
       01 GRADE-NEW PIC 9(1)V9(2).     
       01 SUM-GRADE PIC 9(3)V9(3) .
       01 SUM-SUB-UNIT PIC 9(3)V999 .
       01  SUM-GRADE-AVG PIC 9(1)V9(3).
       01  SUB-SCI-ID            PIC X(1).
       01  SUB-CS-ID             PIC X(2).

      
      *SCI
       01  CREDIT-SCI         PIC 9(3).
       01  SUM-AVG-SCI    PIC 9(1)V9(3).
       01  SUM-CREDIT-SCI PIC 9(3)V9(3).

      *COMSCI
       01  CREDIT-CS            PIC 9(3).
       01  SUM-AVG-CS       PIC 9(1)V9(3).
       01  SUM-CREDIT-CS    PIC 9(3)V9(3).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT GRADE-FILE
           OPEN OUTPUT AVG-GRADE-FILE.
           PERFORM UNTIL END-OF-GRADE-FILE
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE 
              END-READ 
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT 
              END-IF 
           END-PERFORM

           DISPLAY "AVG-GRADE : " SUM-GRADE-AVG
           DISPLAY "--------------------"
           DISPLAY "AVG-SCI     = "  SUM-AVG-SCI  
           DISPLAY "--------------------"
           DISPLAY "AVG-COM-SCI = "  SUM-AVG-CS

           MOVE "GRADE-AVG     : " TO SUBJECT-NAME
           MOVE SUM-GRADE-AVG   TO AVG-GRADE IN AVG-GRADE-DATAILS
           WRITE AVG-GRADE-DATAILS

           MOVE "GRADE-AVG-SCI : " TO SUBJECT-NAME
           MOVE SUM-AVG-SCI  TO AVG-GRADE IN AVG-GRADE-DATAILS
           WRITE AVG-GRADE-DATAILS

           MOVE "GRAD-EAVG-CS : " TO SUBJECT-NAME
           MOVE SUM-AVG-CS   TO AVG-GRADE IN AVG-GRADE-DATAILS
           WRITE AVG-GRADE-DATAILS

           CLOSE GRADE-FILE 
           CLOSE AVG-GRADE-FILE 
           GOBACK 
           .

       001-PROCESS. 
           
           COMPUTE SUM-SUB-UNIT = SUB-UNIT +SUM-SUB-UNIT 
           COMPUTE SUM-GRADE = SUM-GRADE + GRADE-NEW * SUB-UNIT  
           COMPUTE SUM-GRADE-AVG = SUM-GRADE / SUM-SUB-UNIT
           EVALUATE TRUE 
              WHEN GRADE = "A" MOVE  4.00   TO GRADE-NEW
              WHEN GRADE = "B+" MOVE 3.50   TO GRADE-NEW
              WHEN GRADE = "B" MOVE  3.00   TO GRADE-NEW
              WHEN GRADE = "C+" MOVE 2.50    TO GRADE-NEW
              WHEN GRADE = "C" MOVE  2.00   TO GRADE-NEW
              WHEN GRADE = "D+" MOVE 1.50   TO GRADE-NEW
              WHEN GRADE = "D" MOVE  1.00   TO GRADE-NEW
           END-EVALUATE
           .

           MOVE SUB-ID TO SUB-SCI-ID
           IF SUB-SCI-ID IS EQUAL TO "3" THEN
              COMPUTE CREDIT-SCI = CREDIT-SCI + SUB-UNIT 
              COMPUTE SUM-CREDIT-SCI = SUM-CREDIT-SCI 
              +(SUB-UNIT * GRADE-NEW) 
              COMPUTE SUM-AVG-SCI = SUM-CREDIT-SCI 
              / CREDIT-SCI  
           END-IF.
           
           MOVE SUB-ID TO SUB-CS-ID
           IF SUB-CS-ID IS EQUAL TO "31" THEN
              COMPUTE CREDIT-CS = CREDIT-CS + SUB-UNIT 
              COMPUTE SUM-CREDIT-CS = SUM-CREDIT-CS +(SUB-UNIT * 
              GRADE-NEW) 
              COMPUTE SUM-AVG-CS = SUM-CREDIT-CS / CREDIT-CS  
           END-IF.

           
       001-EXIT.
           EXIT.

    